#!/usr/bin/python3
# -*- coding: utf-8 -*-

import random #SE IMPORTA RANDOM PARA PODER RELLENAR LAS LISTAS CON LOS CRITERIOS SOLICITADOS 
import time #SE IMPORTA TIME PARA GENERAR UN DELAY A LA HORA DE VOLVER A IMPRIMIR LOS CAMBIOS DE LA MATRIZ
import os #SE IMPORTA OS PARA LIMPIAR LA MATRIZ CADA VEZ QUE SE IMPRIMA

matriz_inicial=[] #SE CREA UNA LISTA EN LA CUAL SE CREAR A SU VEZ OTRAS LISTAS, ESTO GENERARA QUE SE FORME UNA MATRIZ DE LISTAS POR LISTAS
matriz_temporal=[] 
N=int(input("Ingrese el tamaño de la matriz cuadrada: ")) #N = INDICE, TAMAÑO DE LA MATRIZ

def celulas_random (matriz_inicial, N): #FUNCION QUE RELLENA LA MATRIZ CON UN NUMERO ALEATORIOS DE CELULAS VIVAS Y MUERTAS

	for i in range (N):
		matriz_inicial.append(0)
		matriz_inicial[i] = []
		for j in range (N):	
			matriz_inicial[i].append("01"[random.randrange(2)])
		
# 1 = CELULAS VIVAS, ENTONCES SE REPRESENTA CON LA LETRA X
# 0 = CELULAR MUERTAS, ENTONCES SE REPRESENTA CON UN ESPACIO EN BLANCO

def rangos (matriz_inicial, i, j):
	contador=1
	
	if i < 0:
		contador = 0
	elif j < 0:
		contador = 0
	elif i == N:
		contador = 0
	elif j == N:
		contador = 0
		
	return contador
	
	
	
def celulas_vecinas_vivas(matriz_inicial, N, i, j): #FUNCION QUE RECORRE LA MATRIZ INICIAL CONTANDO LA CANTIDAD DE CELULAS VECINAS VIVAS

	contador_vecinas_vivas=0 #SE INICIA EL CONTADOR EN CERO PARA QUE NO CONTENGA RESIDUOS 
	
	if(rangos(matriz_inicial, i-1, j-1)==1 and matriz_inicial[i-1][j-1]==1):
		contador_vecinas_vivas += 1
	if(rangos(matriz_inicial, i-1, j)==1 and matriz_inicial[i-1][j]==1):
		contador_vecinas_vivas += 1
	if(rangos(matriz_inicial, i-1, j+1)==1 and matriz_inicial[i-1][j+1]==1):
		contador_vecinas_vivas += 1
	if(rangos(matriz_inicial, i, j-1)==1 and matriz_inicial[i][j-1]==1):
		contador_vecinas_vivas += 1
	if(rangos(matriz_inicial, i, j+1)==1 and matriz_inicial[i][j+1]==1):
		contador_vecinas_vivas += 1
	if(rangos(matriz_inicial, i+1, j-1)==1 and matriz_inicial[i+1][j-1]==1):
		contador_vecinas_vivas += 1
	if(rangos(matriz_inicial, i+1, j)==1 and matriz_inicial[i+1][j]==1):
		contador_vecinas_vivas += 1
	if(rangos(matriz_inicial, i+1, j+1)==1 and matriz_inicial[i+1][j+1]==1):
		contador_vecinas_vivas += 1
									
	return contador_vecinas_vivas	
	
	
def creacion_matriz_temporal(matriz_temporal, N):

	for i in range (N):
		matriz_temporal.append(0)
		matriz_temporal[i] = []
		for j in range (N):	
			matriz_temporal[i].append(0)


				
#• Cada célula con 1 o ninguna célula vecina viva muere por soledad.
#• Cada célula muerta con 3 células vecinas vivas nace.
def celulas_condiciones (matriz_inicial, matriz_temporal, N): #FUNCION QUE RECORRE LA MATRIZ INICIAL GENERANDO UNA TEMPORAL QUE CONTENGA EL CAMBIO RESTRINGIDO POR LAS CONDICIONES INDICADAS
						
	contador_vecinas_vivas = celulas_vecinas_vivas(matriz_inicial, N, i, j)
	contador_vivas=0
	contador_muertas=0
	
	
	while True: 
		
		for i in range (N):
			for j in range (N):
				if(matriz_inicial[i][j]==0):
					if(contador_vecinas_vivas == 3):
						matriz_temporal[i][j]==1
						contador_vivas += 1
				elif(matriz_inicial[i][j]==1):
					if(contador_vecinas_vivas==2 or contador_vecinas_vivas==3):
						matriz_temporal[i][j]==1
						contador_vivas += 1
					if(contador_vecinas_vivas >= 4):
						matriz_temporal[i][j]==0
						contador_muertas += 1
					if(contador_vecinas_vivas <=1):
						matriz_temporal[i][j]==0
						contador_muertas += 1
		print ("Celulas VIVAS actuales: ", contador_vivas)
		print ("Celulas MUERTAS actuales: ", contador_muertas)
		
		if contador_vecinas_vivas == 0:
			break
			
			
	

def impresora(matriz, N):

	
	for i in range (N): #RECORRE LAS FILAS
		for j in range (N):	#DENTRO DE CADA FILA, EN CADA POSICION DE COLUMNA EVALUA EL VALOR CAMBIANDO EL SIMBOLO DE ESTOS
			if (matriz[i][j]=="1"):
				print("| X", end=" ") 
			else:
				print("|  ", end=" ")	
		print(" |")
		
	
						
celulas_random(matriz_inicial, N)
impresora(matriz_inicial, N)
creacion_matriz_temporal(matriz_temporal, N)


while True: 
	for i in range (N):
		for j in range (N):
			celulas_vecinas_vivas(matriz_inicial, N, i , j)
			celulas_condiciones(matriz_inicial, matriz_temporal, N)
	celulas_condiciones(matriz_inicial, matriz_temporal, N)		
	impresora(matriz_temporal, N)
	time.sleep(1)
	os.system("clear")
